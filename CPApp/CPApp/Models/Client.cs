﻿using System.Text;
using System.Net.Sockets;
using Xamarin.Forms;
using System.IO;
using Xamarin.Forms.PlatformConfiguration;
using System;
using Android.Graphics;

namespace CPApp.ViewModels
{
    public static class Client
    {
        static NetworkStream Stream { get; set; }
        static string clientName;
        static TcpClient client;
        static string ServerIpAddr;
        const int port = 8888;
        public delegate void MethodContainer(string message);
        public static event MethodContainer MessageChanged;

        public static string ImgPath;

        private static string msg;
        public static string MSG
        {
            get { return msg; }
            set
            {
                msg = value;
                MessageChanged(value);
            }
        }

        public static void ConnectToServer(string ServerIp)
        {
            ServerIpAddr = ServerIp;
            client = new TcpClient(ServerIp, port);
            Stream = client.GetStream();
        }

        public static void SendDeviceName()
        {
            clientName = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString() + "-" + "Android";
            byte[] message = Encoding.UTF8.GetBytes(clientName);
            Stream.Write(message, 0, message.Length);
            MSG = "Your device name: " + clientName;
        }
        public static void SendMessage(string message)
        {
            byte[] msg_byte = Encoding.UTF8.GetBytes(message);
            Stream.Write(msg_byte, 0, msg_byte.Length);
        }
        

        public static void GetImage()
        {
            try
            {
                if (Directory.Exists(System.IO.Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, "Android", "data", "RFIDInspector", "files", "Pictures")))
                {
                    
                }
                else
                {
                    Directory.CreateDirectory(System.IO.Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, "Android", "data", "RFIDInspector", "files", "Pictures"));
                }
                ImgPath = System.IO.Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, "Android", "data", "RFIDInspector", "files", "Pictures", "user.png");
                if (File.Exists(ImgPath))
                {
                    File.Delete(ImgPath);
                }
                SendMessage("ReadyToGet");
                int length = Convert.ToInt32(GetMessage());
                SendMessage("ReadyToGet");
                byte[] data = new byte[length];
                int bytes = 0;
                do
                {
                    bytes = Stream.Read(data, 0, data.Length);
                }
                while (Stream.DataAvailable);
                File.WriteAllBytes(ImgPath, data);
                
            }
            catch (Exception ex)
            {

            }
        }
        public static string GetMessage()
        {
            string message = GetSocketMessage();
            MSG = "SEREVER: " + message;
            return message;
        }
        static internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
        private static string GetSocketMessage()
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);
            return builder.ToString();
        }
    }
}
