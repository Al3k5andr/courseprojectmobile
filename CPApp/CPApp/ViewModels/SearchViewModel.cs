﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CPApp.ViewModels
{
    class SearchViewModel : BaseViewModel
    {
        private string name;
        private string surname;
        private string patronamic;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }
        public string Patronamic
        {
            get { return patronamic; }
            set
            {
                patronamic = value;
                OnPropertyChanged(nameof(Patronamic));
            }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                ValidationResult = (Message != null && RFID_regex.IsMatch(Message)) ? true : false;
                OnPropertyChanged(nameof(Message));
            }
        }
        private string servermessage;
        public string ServerMessage
        {
            get { return servermessage; }
            set
            {
                servermessage = value;
                OnPropertyChanged(nameof(ServerMessage));
            }
        }

        private string image = "@drawable/noPhoto";
        public  string IMG
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged(nameof(IMG));
            }
        }

        private string erroreMsg;
        public string ErroreMsg 
        {
            get { return erroreMsg; }
            set
            {
                erroreMsg = value;
                OnPropertyChanged(nameof(ErroreMsg));
            }
        }




        public ICommand SendMsg
        {
            get
            {
                return new Command((obj) =>
                {
                    try
                    {
                        Name = "Name:";
                        Surname = "Surname:";
                        Patronamic = "Patronamic:";

                        IMG = "@drawable/noPhoto";
                        Client.SendMessage(Message);
                        
                        string[] FIO = Client.GetMessage().Split(new char[] { '-' });
                        ErroreMsg = (FIO[0] == "NO SUCH RFID IN DATABASE") ? FIO[0] : null;
                        
                        Name = (ErroreMsg == null)? "Name: " + FIO[0]: "Name: ";
                        Surname = "Surname: " + FIO[1];
                        Patronamic = "Patronamic:" + FIO[2];
                        Client.GetImage();
                        IMG = Client.ImgPath;
                        ErroreMsg = null;
                    }
                    catch (Exception ex)
                    {
                        ErroreMsg = "Something go wrong =(";
                    }
                });
            }
        }
        public SearchViewModel()
        {
            Name = "Name:";
            Surname = "Surname:";
            Patronamic = "Patronamic:";
        }


        //---------------------Validate---------------->


        private bool validationResult = false;
        public bool ValidationResult
        {
            get { return validationResult; }
            set
            {
                validationResult = value;
                OnPropertyChanged(nameof(ValidationResult));
            }
        }
        Regex RFID_regex = new Regex(@"\d{10}");

    }
}
