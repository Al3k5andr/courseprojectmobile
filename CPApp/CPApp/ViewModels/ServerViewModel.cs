﻿using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System;

namespace CPApp.ViewModels
{
    class ServerViewModel : BaseViewModel
    {
        public ObservableCollection<string> LogList { get; set; }// Лог сервера

        public ObservableCollection<string> List = new ObservableCollection<string>();
        public ICommand Connect
        {
            get
            {
                return new Command((obj) =>
                {
                    try
                    {
                        LogList = null;
                        List = new ObservableCollection<string>();
                        Client.MessageChanged += Client_MessageChanged;
                        Client.ConnectToServer(IP);
                        Status = "Connected";
                        Client.SendDeviceName();
                    }
                    catch(Exception ex)
                    {
                        Client_MessageChanged("Errore: I cant connect to this server");
                    }
                });
            }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set
            {
                message = value;

                OnPropertyChanged(message);
            }
        }

        public void Update()
        {
            LogList = new ObservableCollection<string>();
            foreach (string item in List)
                LogList.Add(item);
            OnPropertyChanged(nameof(LogList));
        }
        private void Client_MessageChanged(string message)
        {
            Message = message;
            List.Add(message);
            Update();
        }

        private string ip;
        public string IP
        {
            get { return ip; }
            set
            {
                ip = value;
                ValidationResult = (ip_regex.IsMatch(IP) && IP != null) ? true : false;
                OnPropertyChanged(nameof(IP));
            }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged(nameof(Status));
            }
        }



        //----------------VALIDATION------------------//
        private bool validationResult = false;
        public bool ValidationResult
        {
            get { return validationResult; }
            set
            {
                validationResult = value;
                OnPropertyChanged(nameof(ValidationResult));
            }
        }
        
            Regex ip_regex = new Regex(@"(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}");
    }

}
