﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CPApp.ViewModels;

namespace CPApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Search : ContentPage
	{
		public Search ()
		{
			InitializeComponent();
            this.BindingContext = new SearchViewModel();
        }
 
    }
}