﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CPApp.ViewModels;

namespace CPApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Server : ContentPage
	{
		public Server ()
		{
			InitializeComponent ();
            this.BindingContext = new ServerViewModel();
        }
	}
}